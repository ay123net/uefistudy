@echo off

REM /**
REM  * @file signefi.bat
REM  * 
REM  * Batch file to sign efi file.
REM  * 
REM  * @author https://ay123.net 
REM  * @version 0.1
REM  * @date 2024-03-02
REM  * 
REM  * @copyright Copyright (c) 2015 - 2024
REM  * 
REM  */

SETLOCAL

:: 获取当前工作目录
SET "currentDirectory=%cd%"

:: 原始的EFI文件路径
SET "originalEfiFilePath=%currentDirectory%\hello.efi"
:: 新的EFI文件路径
SET "newEfiFilePath=%currentDirectory%\hello_Signed.efi"

:: 首先复制文件
COPY /Y "%originalEfiFilePath%" "%newEfiFilePath%"

:: 然后对新文件进行签名
SET "pfxFilePath=%currentDirectory%\keys\DB.pfx"   :: PFX文件路径
SET "timeserverURL=http://timestamp.digicert.com"  :: 时间戳服务器的URL
SET "pfxPassword=YourPfxPassword"                  :: PFX文件的密码

:: 请确保signtool.exe的路径已经添加到系统的环境变量PATH中，或者使用signtool.exe的完全限定路径
.\tools\signtool\signtool sign /v /fd SHA256 /t "%timeserverURL%" /f "%pfxFilePath%" /p "%pfxPassword%" "%newEfiFilePath%"

ENDLOCAL