#include  <Uefi.h>
#include  <Library/UefiLib.h>
#include  <Library/UefiBootServicesTableLib.h>

#include  <Library/HiiLib.h>
#include  <Library/UefiHiiServicesLib.h>
#include  <Library/MemoryAllocationLib.h>

EFI_GUID    mPackageListGuid = {0xd8ebc548, 0xd7ea, 0x46e6, {0x98, 0x0d, 0x0e, 0xa0, 0x04, 0xfd, 0xc7, 0x00}};

int main (
  IN int Argc,
  IN char **Argv
  )
{
    EFI_HANDLE     HiiHandle;
    EFI_STRING     String;
    EFI_STATUS     Status;
    UINTN          StringSize;
    //CHAR8        * Language = "en-US";
    CHAR8        * Language = "zh-Hans";

    HiiHandle = HiiAddPackages (&mPackageListGuid, gImageHandle, ShellFontStrings, NULL);  

    StringSize = 0;
    Status = gHiiString->GetString(gHiiString, Language, HiiHandle, STRING_TOKEN (STR_TITLE), String, &StringSize, NULL);

    if (Status == EFI_BUFFER_TOO_SMALL) {
        String = AllocateZeroPool (StringSize);
        Status = gHiiString->GetString (gHiiString, Language, HiiHandle, STRING_TOKEN (STR_TITLE), String, &StringSize, NULL);
        if (EFI_ERROR(Status)) return 0;
    }

    Print(L"Test String:%s\n\r", String);

    return(0);
}
