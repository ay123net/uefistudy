#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/ShellLib.h>
#include <Library/DevicePathLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/DevicePath.h>
#include <Protocol/DevicePathToText.h>


#define AY123_DEVPATH_demo_1   1 
#define AY123_DEVPATH_demo_2   0

int main(int argc, char **argv) 
{
#if AY123_DEVPATH_demo_1

    EFI_STATUS                         Status;
    EFI_DEVICE_PATH_PROTOCOL         * DevPath;
    EFI_DEVICE_PATH_TO_TEXT_PROTOCOL * DevPath2Text;
    CHAR16                           * DevPathText;
    Status = gBS->LocateProtocol(&gEfiDevicePathProtocolGuid, NULL, (VOID *) &DevPath);
    
    if (EFI_ERROR(Status)) {
        Print(L"Error %r\n\r", Status);
        return Status;
    }

    Status = gBS->LocateProtocol(&gEfiDevicePathToTextProtocolGuid, NULL, (VOID *) &DevPath2Text);
    if (EFI_ERROR(Status)) {
        Print(L"Error %r\n\r", Status);
        return Status;
    }

    DevPathText = DevPath2Text->ConvertDevicePathToText(DevPath, TRUE, FALSE);

    Print(L"Device path: %s\n\r", DevPathText);

#elif AY123_DEVPATH_demo_2

    EFI_STATUS                         Status;
    EFI_DEVICE_PATH_PROTOCOL         * DevPath;
    EFI_DEVICE_PATH_TO_TEXT_PROTOCOL * DevPath2Text;
    EFI_HANDLE                       * HandleBuffer;
    UINTN                              HandleCount;
    UINTN                              Index;

    Status = gBS->LocateHandleBuffer(
                        ByProtocol,
                        &gEfiSimpleFileSystemProtocolGuid,
                        NULL,
                        &HandleCount,
                        &HandleBuffer
                        );
    if (EFI_ERROR(Status)) {
        Print(L"Error %r\n\r", Status);
        return Status;
    }

    Status = gBS->LocateProtocol(&gEfiDevicePathToTextProtocolGuid, NULL, (VOID *) &DevPath2Text);
    if (EFI_ERROR(Status)) {
        Print(L"Error %r\n\r", Status);
        return Status;
    }

    for (Index = 0; Index < HandleCount; Index ++) {
        Status = gBS->HandleProtocol(HandleBuffer[Index], &gEfiDevicePathProtocolGuid, (VOID *)&DevPath);
        if (EFI_ERROR(Status)) continue;

        while (!IsDevicePathEnd (DevPath)) {
            if (DevPath->Type == MESSAGING_DEVICE_PATH &&
                DevPath->SubType == MSG_USB_DP ) {
                Print (L"Usb storage found:%s\n\r", DevPath2Text->ConvertDevicePathToText(DevPath, TRUE, FALSE));
                break;
            }
            DevPath = NextDevicePathNode(DevPath);
        }
    }

#endif

    return 0;
}