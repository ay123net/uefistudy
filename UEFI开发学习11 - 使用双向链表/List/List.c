/** @file
    A simple, basic, EDK II native, "hello" application to verify that
    we can build applications without LibC.

    Copyright (c) 2010 - 2011, Intel Corporation. All rights reserved.<BR>
    This program and the accompanying materials
    are licensed and made available under the terms and conditions of the BSD License
    which accompanies this distribution. The full text of the license may be found at
    http://opensource.org/licenses/bsd-license.

    THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
    WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/
#include  <Uefi.h>
#include  <Library/UefiLib.h>
#include  <Library/ShellCEntryLib.h>

typedef struct {
    LIST_ENTRY Link;
    UINTN Data;
} TESTLIST;

int main(
  IN int Argc,
  IN char **Argv
)
{
    TESTLIST ListHead, Node1, Node2, Node3, * Node;

    Node1.Data = 13;
    Node2.Data = 14;
    Node3.Data = 15;

    InitializeListHead(&ListHead.Link);

    InsertTailList(&ListHead.Link, &Node1.Link);
    InsertTailList(&ListHead.Link, &Node2.Link);
    InsertTailList(&ListHead.Link, &Node3.Link);

    Node = (TESTLIST *) GetFirstNode (&ListHead.Link);

    while (!IsNull(&ListHead.Link, &Node->Link))
    {
        Print(L"LIST DATA: %d\n\r", Node->Data);
        Node = (TESTLIST*)GetNextNode(&ListHead.Link, &Node->Link);
    }
    
    return 0;
}